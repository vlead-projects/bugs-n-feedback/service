#+TITLE: Data Model
#+AUTHOR: VLEAD
#+DATE: [2020-05-18 Mon]
#+SETUPFILE: ./org-templates/level-0.org
#+TAGS: boilerplate(b)
#+EXCLUDE_TAGS: boilerplate
#+OPTIONS: ^:nil

* Introduction
This document contains the data model of the backend
service.

* Experiment Descriptor
  Each experiment contains a descriptor that contains the
  following fields.
  1. Name of the experiment
  2. Short name of the experiment
  3. Repo URL

  These fields are mandatory for bug-n-feedback service to
  work.  There could be other fields for other uses or
  services like hosting etc.  Name of the experiment is
  unique across all the experiments.

  #+BEGIN_EXAMPLE
  {
   "name": "Performance Characteristics of Centrifugal Pump",
   "short-name": "centrifugal-pump",
   "repo": "https://gitlab.com/vlead-projects/experiments/iit-bombay/performance-characteristics-centrifugal-pump-nitk.git",
    ...
   }
  #+END_EXAMPLE

* Star Rating
  
  An experiment contains multiple artifacts.  The rating is
  captured for each artifact cumulatively.

  The model contains:
  1. Name of the experiment
  2. Name of the artifact
  3. Cumulative rating (0 - 5)

* Problem Report
  Problem Reports are also captured for each artifact.  The
  model contains:
  1. Name of the experiment
  2. Name of the artifact
  3. Bug Description
  4. Screen shot (optional)

